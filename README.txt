# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

https://gitlab.com/hnc/hopp
http://hnc.toile-libre.org/index.php?section=dev&page=hopp
https://www.lri.fr/~bagneres/index.php?section=dev&page=hopp


 ------
| hopp |
 ------

Header-Only Program Parts

Basic (but useful), modern and generic C++14 header-only library

Apache License, Version 2.0
GNU Affero General Public License 3+


 --------------------
| System Requirement |
 --------------------

Required:
- C++11 compiler

Optional:
- CMake build system
- OpenMP
- OpenSSL


 --------------
| Installation |
 --------------
 
With CMake
----------

mkdir build
cd build
cmake .. # -DCMAKE_INSTALL_PREFIX=/path/to/install
         # -DDISABLE_TESTS=TRUE
         # -DCMAKE_BUILD_TYPE=Release
         # -DCMAKE_BUILD_TYPE=Debug
make
# make doxygen
# make test
make install # su -c "make install" # sudo make install
 
Without CMake
-------------
 
This project is a header-only library, you can copy the include directory in /usr/local (for example) or in your project. (But you have to define some macros to enable optional parts.)


 -------------
| Utilization |
 -------------
 
If you use CMake, add these lines in your CMakeLists.txt:
# hopp
message(STATUS "---")
find_package(hopp REQUIRED)
# See /installation/path/lib/hopp/hopp-config.cmake for CMake variables
