// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/int/uint128.hpp>
#include <hopp/int/ulint.hpp>


int main()
{
	std::cout << "Test #include <hopp/uint128.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	for (hopp::ulint i = 0; i < 10; ++i)
	{
		for (hopp::ulint j = 0; j < 10; ++j)
		{
			hopp::uint128 a = i;
			hopp::uint128 b = j;
			
			++nb_test;
			{
				auto const r = a + b;
				std::cout << a << " + " << b << " " << r << std::endl;
				auto const s = i + j;
				nb_test -= hopp::test(r != s, hopp::to_string(a, " + ", b, " = ", r, " fails, expected result = ", s, "\n"));
			}
		}
	}
	
	hopp::test(nb_test == 0, "hopp::uint128: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
