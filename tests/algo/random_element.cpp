// Copyright © 2014, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <list>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/algo/random_element.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/algo/random_element.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	// std::vector<int>
	
	++nb_test;
	{
		std::vector<int> c({4, 8, 7, 6, 2});
		std::cout << c << std::endl;
		
		auto & r = hopp::random_element(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<int> const c({4, 8, 7, 6, 2});
		std::cout << c << std::endl;
		
		auto const & r = hopp::random_element(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<int> const c({4, 8, 7, 6, 2});
		std::cout << c << std::endl;
		
		auto r = hopp::random_element_copy(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	// std::list<std::string>
	
	++nb_test;
	{
		std::list<std::string> c({"A", "std::list", "of", "std::string"});
		std::cout << c << std::endl;
		
		auto & r = hopp::random_element(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<std::string> c({"A", "std::list", "of", "std::string"});
		std::cout << c << std::endl;
		
		auto const & r = hopp::random_element(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<std::string> c({"A", "std::list", "of", "std::string"});
		std::cout << c << std::endl;
		
		auto r = hopp::random_element_copy(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	// std::string
	
	++nb_test;
	{
		std::string c("A std::string");
		std::cout << c << std::endl;
		
		auto & r = hopp::random_element(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string c("A std::string");
		std::cout << c << std::endl;
		
		auto const & r = hopp::random_element(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string c("A std::string");
		std::cout << c << std::endl;
		
		auto r = hopp::random_element_copy(c);
		std::cout << r << std::endl;
		
		nb_test -= hopp::test(std::find(c.begin(), c.end(), r) != c.end(), "hopp::random_element fails with " + hopp::to_string(c) + "\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::random_element: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
