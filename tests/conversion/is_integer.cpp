// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>
#include <list>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/conversion/is_integer.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/conversion/is_integer.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	nb_test++;
	{
		std::string s = "42";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == true, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = " 42";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == false, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = "42 ";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == false, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = "-7";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == true, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = "+73";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == true, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = "- 42";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == false, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = "+";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == false, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::string s = "-";
		std::cout << "'" << s << "'" << std::endl;
		
		nb_test -= hopp::test(hopp::is_integer(s) == false, "hopp::is_integer fails\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::is_integer: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
