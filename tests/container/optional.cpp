// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/container/optional.hpp>


int main()
{
	std::cout << "Test #include <hopp/container/optional.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		hopp::optional<int> o;
		std::cout << o << std::endl;
		
		nb_test -= hopp::test(o == false, "hopp::optional<int> o; fails!\n");
	}
	
	++nb_test;
	{
		hopp::optional<int> o = 7;
		std::cout << o << std::endl;
		
		nb_test -= hopp::test(o == true && *o == 7, "hopp::optional<int> o = 7; fails!\n");
	}
	
	++nb_test;
	{
		hopp::optional<std::string> o("A std::string");
		std::cout << o << std::endl;
		
		nb_test -= hopp::test(o == true && *o == "A std::string", "hopp::optional<std::string> o = \"A std::string\"; fails!\n");
	}
	
	nb_test += 2;
	{
		double d = 21.42;
		hopp::optional<double &> o = d;
		std::cout << o << std::endl;
		
		nb_test -= hopp::test(o == true && *o == 21.42, "hopp::optional<double &> o = d; fails!\n");
		
		*o = 73;
		std::cout << d << std::endl;
		nb_test -= hopp::test(d == 73.0, "*o = 73; fails!\n");
	}
	
	hopp::test(nb_test == 0, "hopp::optional: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
