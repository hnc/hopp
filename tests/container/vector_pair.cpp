// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <vector>
#include <string>
#include <typeinfo>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/container/vector_pair.hpp>
#include <hopp/literal/_s.hpp>


int main()
{
	std::cout << "Test #include <hopp/container/vector_pair.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	nb_test++;
	{
		hopp::vector_pair<std::string, std::string> pairs;
		
		pairs.emplace_back("A vector_pair", "with");
		pairs.push_back("std::string", "as keys");
		pairs.push_back(std::make_pair("and std::string"_s, "values"_s));
		pairs.insert("with", "an interface");
		pairs.insert(std::make_pair("like"_s, "std::vector<std::pair<key_t, T>>"_s));
		pairs["and"] = "std::map<key_t, T>";
		
		std::cout << pairs << std::endl;
		
		nb_test -= hopp::test(pairs == hopp::vector_pair<std::string, std::string>({ { "A vector_pair", "with" }, { "std::string", "as keys" }, { "and std::string", "values" }, { "with", "an interface" }, { "like", "std::vector<std::pair<key_t, T>>" }, { "and", "std::map<key_t, T>" } }), "hopp::vector_pair fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		hopp::vector_pair<std::string, int> pairs;
		
		pairs["one"] = 1; // Create "one" key with 1 value
		pairs["two"] = 2;
		pairs["three"] = 4;
		pairs["three"] = 3; // Replace value at key "three"
		
		std::cout << pairs << std::endl; // { { one, 1 }, { two, 2 }, { three, 3 } }
		
		nb_test -= hopp::test(pairs == hopp::vector_pair<std::string, int>({ { "one", 1 }, { "two", 2 }, { "three", 3 } }), "hopp::vector_pair fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::vector<std::pair<std::string, std::string>> vector_of_pairs = { { "test", "constructor" }, { "from", "std::vector" } };
		
		hopp::vector_pair<std::string, std::string> pairs = vector_of_pairs;
		
		std::cout << pairs << std::endl;
		
		nb_test -= hopp::test(pairs == hopp::vector_pair<std::string, std::string>({ { "test", "constructor" }, { "from", "std::vector" } }), "hopp::vector_pair fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		hopp::vector_pair<std::string, std::string> pairs = { { "test", "constructor" }, { "from", "initializer list" } };
		
		std::cout << pairs << std::endl;
		
		nb_test -= hopp::test(pairs == hopp::vector_pair<std::string, std::string>({ { "test", "constructor" }, { "from", "initializer list" } }), "hopp::vector_pair fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::vector<std::pair<int, std::string>> vector_of_pairs = { { -1, "minus one" }, { 1, "one" }, { 2, "two" }, { 3, "three" } };
		
		hopp::vector_pair<int, std::string> pairs(vector_of_pairs.begin() + 1, vector_of_pairs.end() - 1);
		
		std::cout << pairs << std::endl;
		
		nb_test -= hopp::test(pairs == hopp::vector_pair<int, std::string>({ { 1, "one" }, { 2, "two" } }), "hopp::vector_pair fails\n");
	}
	std::cout << std::endl;
	
	// Should not compile
	{
		//hopp::vector_pair<int, std::string> pairs(1, 2);
	}
	
	hopp::test(nb_test == 0, "hopp::vector_pair: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
