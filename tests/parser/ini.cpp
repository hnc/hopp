// Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/parser/ini.hpp>
#include <hopp/filesystem/filename.hpp>


int main(int argc, char * * argv)
{
	if (argc == 1)
	{
		std::cerr << "Usage: " << argv[0] << " <pathname.ini>" <<  std::endl;
		return 1;
	}
	
	std::string const pathname = argv[1];
	
	std::cout << "Test #include <hopp/parser/ini.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	nb_test++;
	{
		auto const r = hopp::parser::ini(pathname);
		
		std::cout << pathname << " parsed =" << std::endl;
		std::cout << r << std::endl;
		
		if (hopp::filesystem::filename(pathname) == "ini_wikipedia_en.ini")
		{
			hopp::vector_pair<std::string, hopp::vector_pair<std::string, std::string>> test;
			test["owner"].push_back("name", "John Doe");
			test["owner"].push_back("organization", "Acme Widgets Inc.");
			test["database"].push_back("server", "192.0.2.62");
			test["database"].push_back("port", "143");
			test["database"].push_back("file", "\"payroll.dat\"");
			
			nb_test -= hopp::test(r == test, "hopp::parser::ini(" + pathname + ") fails\n");
		}
		else if (hopp::filesystem::filename(pathname) == "ini_wikipedia_fr.ini")
		{
			hopp::vector_pair<std::string, hopp::vector_pair<std::string, std::string>> test;
			test["owner"].push_back("name", "juan dona");
			test["owner"].push_back("organization", "cablage ideal");
			test["database"].push_back("server", "192.0.2.42");
			test["database"].push_back("port", "143");
			test["database"].push_back("file", "\"acme payroll.dat\"");
			
			nb_test -= hopp::test(r == test, "hopp::parser::ini(" + pathname + ") fails\n");
		}
		else
		{
			--nb_test;
			std::cerr << pathname << " is not supported for test" << std::endl;
		}
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::vector_pair: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
